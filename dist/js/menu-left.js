'use strict';

btnLeftMenu.addEventListener('click', function(event){

    if(layout.classList.contains('menu-left--open')){
        layout.classList.remove('menu-left--open');
    } else {
        layout.classList.add('menu-left--open');
    }
});

overlay.addEventListener('click', function(event){

    layout.classList.remove('menu-left--open');
});